<?php

namespace Drupal\Tests\commerce_events\Traits;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_order\Entity\OrderItemType;
use Drupal\commerce_order\Entity\OrderType;
use Drupal\commerce_product\Entity\Product;
use Drupal\commerce_product\Entity\ProductType;
use Drupal\commerce_product\Entity\ProductVariation;
use Drupal\commerce_product\Entity\ProductVariationType;
use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\user\Traits\UserCreationTrait;

trait CommerceEventProductTrait {

  /**
   * Create an event product variation type.
   *
   * @param array $values
   *   (optional) An array of values to merge with the default ones. Defaults
   *   to an empty array.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface
   *   The event variation type.
   */
  public function createEventProductVariationType(array $values = []) {
    $variation_type = ProductVariationType::create(array_merge([
      'id' => 'event_variation',
      'label' => 'Event variation',
      'orderItemType' => 'event',
      'generateTitle' => FALSE,
    ], $values));

    $variation_type->save();

    return $variation_type;
  }

  /**
   * Create an event product type.
   *
   * @param array $values
   *   (optional) An array of values to merge with the default ones. Defaults
   *   to an empty array.
   *
   * @return \Drupal\commerce_product\Entity\ProductTypeInterface
   *   The event product type.
   */
  public function createEventProductType($values = []) {
    $product_type = ProductType::create(array_merge([
      'label' => 'Event',
      'id' => 'event',
      'variationType' => 'event',
    ], $values));

    $product_type->save();

    commerce_product_add_variations_field($product_type);

    return $product_type;
  }

  /**
   * Create an event product.
   *
   * @param array $values
   *   (optional) An array of values to merge with the default ones. Defaults
   *   to an empty array.
   *
   * @return \Drupal\commerce_product\Entity\ProductInterface
   *   An event product.
   */
  public function createEventProduct(array $values = []) {
    $variation1 = ProductVariation::create([
      'title' => $this->randomString(),
      'type' => 'event_variation',
    ]);
    $variation1->save();

    $variation2 = ProductVariation::create([
      'title' => $this->randomString(),
      'type' => 'event_variation',
    ]);
    $variation2->save();

    $variation3 = ProductVariation::create([
      'title' => $this->randomString(),
      'type' => 'event_variation',
    ]);
    $variation3->save();

    $event = Product::create(array_merge([
      'title' => $this->randomString(),
      'type' => 'event',
      'variations' => [$variation1, $variation2, $variation3]
    ], $values));

    $event->save();

    return $event;
  }

  public function createEventOrderItemType() {
    $order_item_type = OrderItemType::create([
      'id' => 'event',
      'label' => 'Event',
      'orderType' => 'default',
      'purchasableEntityType' => 'commerce_product_variation',
    ]);

    $order_item_type->save();

    $this->assertInstanceOf(OrderItemType::class, $order_item_type);

    return $order_item_type;
  }

  public function createEventOrderItem(array $values = []) {
    $order_item = OrderItem::create(array_merge([
      'purchased_entity' => [],
      'type' => 'event',
    ], $values));

    $order_item->save();

    $this->assertInstanceOf(OrderItem::class, $order_item);

    return $order_item;
  }

  public function createEventOrderType(array $values = []) {
    $order_type = OrderType::create(array_merge([
      'id' => 'event',
    ], $values));

    $order_type->save();

    $this->assertInstanceOf(OrderType::class, $order_type);

    commerce_order_add_order_items_field($order_type);

    return $order_type;
  }

  public function createEventOrder(array $values = [], $order_items = [], AccountInterface $user = NULL) {
    if (is_null($user)) {
      $user = $this->createUser();
    }

    $order = Order::create(array_merge([
      'mail' => $user->getEmail(),
      'order_items' => $order_items,
      'state' => 'completed',
      'store_id' => $this->store,
      'type' => 'event',
      'uid' => $user->id(),
    ], $values));

    $order->save();

    $this->assertInstanceOf(Order::class, $order);

    return $order;
  }

}
