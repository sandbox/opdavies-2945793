<?php

namespace Drupal\Tests\commerce_events\Kernel;

use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;
use Drupal\Tests\commerce_events\Traits\CommerceEventProductTrait;

class CommerceEventsKernelTestBase extends CommerceKernelTestBase {

  use CommerceEventProductTrait;

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'commerce',
    'commerce_cart',
    'commerce_checkout',
    'commerce_events',
    'commerce_order',
    'commerce_price',
    'commerce_product',
    'commerce_store',
    'entity_reference_revisions',
    'field',
    'inline_entity_form',
    'path',
    'profile',
    'state_machine',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_product');
    $this->installEntitySchema('commerce_product_variation');
    $this->installEntitySchema('user');

    $this->createEventProductVariationType();
    $this->createEventProductType();
    $this->createEventOrderItemType();
    $this->createEventOrderType();
  }

}
