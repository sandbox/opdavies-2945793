# Commerce Events

A helper module relating to events (products).

## Getting Attendees for an Event

```php
/** @var \Drupal\commerce_product\Entity\Product $event */
$event = Product::load($product_id);

/** @var \Drupal\commerce_events\Service\EventManager $event_manager */
$event_manager = \Drupal::service('commerce_events.event_manager');
$event_manager->setEvent($event);

/** @var \Illuminate\Support\Collection $attendees */
$attendees = $event_manager->getAttendees();
```
