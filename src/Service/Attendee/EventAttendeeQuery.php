<?php

namespace Drupal\commerce_events\Service\Attendee;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserInterface;

/**
 * A wrapper around EntityTypeManager for retrieving event attendees.
 */
class EventAttendeeQuery {

  /**
   * @var \Drupal\Core\Entity\EntityStorageInterface $storage
   */
  private $storage;

  /**
   * @var \Drupal\Core\Entity\Query\QueryInterface
   */
  private $query;

  /**
   * User entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $userStorage;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $currentUser;

  /**
   * @var \Drupal\commerce_product\Entity\ProductInterface $event
   */
  private $event;

  /**
   * Constructs a new \Drupal\commerce_events\Service\Attendee\EventAttendeeQuery object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user) {
    $this->storage = $entity_type_manager->getStorage('commerce_order_item');
    $this->query = $this->storage->getQuery();
    $this->userStorage = $entity_type_manager->getStorage('user');
    $this->currentUser = $current_user;
  }

  /**
   * Set the event.
   *
   * @param \Drupal\commerce_product\Entity\ProductInterface $event
   *   The event object.
   *
   * @return $this
   */
  public function setEvent(ProductInterface $event) {
    $this->event = $event;

    $this->query->condition('purchased_entity', $event->getVariationIds(), 'IN');

    return $this;
  }

  /**
   * Add a condition to the query or a condition group.
   *
   * For example, to find all entities containing both the Turkish 'merhaba'
   * and the Polish 'siema' within a 'greetings' text field:
   * @code
   *   $entity_ids = \Drupal::entityQuery($entity_type)
   *     ->condition('greetings', 'merhaba', '=', 'tr')
   *     ->condition('greetings.value', 'siema', '=', 'pl')
   *     ->execute();
   * @endcode
   *
   * @param $field
   *   Name of the field being queried. It must contain a field name, optionally
   *   followed by a column name. The column can be the reference property,
   *   usually "entity", for reference fields and that can be followed
   *   similarly by a field name and so on. Additionally, the target entity type
   *   can be specified by appending the ":target_entity_type_id" to "entity".
   *   Some examples:
   *   - nid
   *   - tags.value
   *   - tags
   *   - tags.entity.name
   *   - tags.entity:taxonomy_term.name
   *   - uid.entity.name
   *   - uid.entity:user.name
   *   "tags" "is the same as "tags.value" as value is the default column.
   *   If two or more conditions have the same field names they apply to the
   *   same delta within that field. In order to limit the condition to a
   *   specific item a numeric delta should be added between the field name and
   *   the column name.
   *   @code
   *   ->condition('tags.5.value', 'news')
   *   @endcode
   *   This will require condition to be satisfied on a specific delta of the
   *   field. The condition above will require the 6th value of the field to
   *   match the provided value. Further, it's possible to create a condition on
   *   the delta itself by using '%delta'. For example,
   *   @code
   *   ->condition('tags.%delta', 5)
   *   @endcode
   *   will find only entities which have at least six tags. Finally, the
   *   condition on the delta itself accompanied with a condition on the value
   *   will require the value to appear in the specific delta range. For
   *   example,
   *   @code
   *   ->condition('tags.%delta', 0, '>'))
   *   ->condition('tags.%delta.value', 'news'))
   *   @endcode
   *   will only find the "news" tag if it is not the first value. It should be
   *   noted that conditions on specific deltas and delta ranges are only
   *   supported when querying content entities.
   * @param $value
   *   The value for $field. In most cases, this is a scalar and it's treated as
   *   case-insensitive. For more complex operators, it is an array. The meaning
   *   of each element in the array is dependent on $operator.
   * @param $operator
   *   Possible values:
   *   - '=', '<>', '>', '>=', '<', '<=', 'STARTS_WITH', 'CONTAINS',
   *     'ENDS_WITH': These operators expect $value to be a literal of the
   *     same type as the column.
   *   - 'IN', 'NOT IN': These operators expect $value to be an array of
   *     literals of the same type as the column.
   *   - 'BETWEEN': This operator expects $value to be an array of two literals
   *     of the same type as the column.
   * @param $langcode
   *   Language code (optional). If omitted, any translation satisfies the
   *   condition. However, if two or more conditions omit the langcode within
   *   one condition group then they are presumed to apply to the same
   *   translation. If within one condition group one condition has a langcode
   *   and another does not they are not presumed to apply to the same
   *   translation.
   *
   * @see \Drupal\Core\Entity\Query\QueryInterface
   *
   * @return $this
   */
  public function condition($field, $value = NULL, $operator = NULL, $langcode = NULL) {
    $this->query->condition($field, $value, $operator, $langcode);

    return $this;
  }

  /**
   * Get the filtered order items for this event.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function getOrderItems() {
    return $this->storage->loadMultiple(
      $this->query->execute()
    );
  }

  /**
   * Get the user IDs for all event attendees.
   *
   * @param bool $exclude_current_user
   *   (optional) Whether to exclude the current user. Defaults to FALSE.
   *
   * @return \Illuminate\Support\Collection
   *   The event attendee IDs.
   */
  public function getAttendeeIds($exclude_current_user = FALSE) {
    $attendees = collect($this->getOrderItems())
      // Remove any order items that do not link to an order.
      ->filter(function (OrderItemInterface $order_item) {
        return $order_item->getOrder() !== NULL;
      })
      // Return an associative array with the user IDs as the keys and the
      // user objects as the values.
      ->map(function (OrderItemInterface $order_item) {
        return $order_item->getOrder()->getCustomer()->id();
      });

    if ($exclude_current_user) {
      // Remove the item with the current user ID as the key.
      if ($position = $attendees->search($this->currentUser->id())) {
        return $attendees->except($position);
      }
    }

    return $attendees;
  }

  /**
   * Get User objects for all event attendees.
   *
   * @param bool $exclude_current_user
   *   (optional) Whether to exclude the current user. Defaults to FALSE.
   *
   * @return \Illuminate\Support\Collection
   *   The event attendee IDs.
   */
  public function getAttendees($exclude_current_user = FALSE) {
    return collect($this->userStorage->loadMultiple(
      $this->getAttendeeIds($exclude_current_user)->all()
    ));
  }

}
