<?php

namespace Drupal\commerce_events\Service;

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_events\Service\Attendee\EventAttendeeQuery;
use Exception;

/**
 * Provides a wrapper for Commerce products that provide helper methods.
 */
class EventManager {

  /**
   * @var \Drupal\commerce_events\Service\Attendee\EventAttendeeQuery $query
   */
  protected $query;

  /**
   * @var \Drupal\commerce_product\Entity\ProductInterface $event
   */
  protected $event;

  /**
   * Constructs a new \Drupal\Core\Controller\FormController object.
   *
   * @param \Drupal\commerce_events\Service\Attendee\EventAttendeeQuery $query
   *   An event attendee query.
   */
  public function __construct(EventAttendeeQuery $query) {
    $this->query = $query;
  }

  /**
   * @param \Drupal\commerce_product\Entity\ProductInterface $event
   *
   * @return $this
   */
  public function setEvent(ProductInterface $event) {
    $this->event = $event;

    return $this;
  }

  /**
   * Return the attendees for an event.
   *
   * @param bool $exclude_current_user
   *   (optional) Whether to exclude the current user. Defaults to FALSE.
   *
   * @return \Illuminate\Support\Collection
   *   The event attendees.
   */
  public function getAttendees($exclude_current_user = FALSE) {
    try {
      return $this->query
        ->setEvent($this->event)
        ->getAttendees($exclude_current_user);
    }
    catch (Exception $e) {
      return collect();
    }
  }

}
